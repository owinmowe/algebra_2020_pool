#ifndef CUSTOM_MATH
#define CUSTOM_MATH

#include "raylib.h"

float customAbs(float f);

float distance2D(Vector2 point1, Vector2 point2);

#endif