#ifndef HOLE_H
#define HOLE_H

#include "raylib.h"
const float HOLE_BASE_RADIUS = 40;

class Hole
{
private:
	Vector2 position;
	float radius;
	Color color;
public:

	Hole();
	void draw();
	Vector2 getPosition();
	void setPosition(Vector2 pos);
	float getRadius();
	void setRadius(float radius);
	Color getColor();
	void setColor(Color col);

};

#endif // !HOLE_H

