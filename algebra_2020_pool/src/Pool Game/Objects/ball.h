#ifndef BALL_H
#define BALL_H

#include "raylib.h"

const int BALL_BASE_RADIUS = 20;
const float BALL_MASS = 160.f;
const float BALL_FRICTION_COEFICIENT = 0.99f;
const float BALL_STOP_SPEED = 0.3f;
const float BALL_WHITE_SHOOT_SPEED_MULTIPLIER = 2.f;
const float SQRT_THREE = 1.7320508075f;
const float BALLS_STARTING_POSITION = 0.125f;
const float WHITE_BALL_STARTING_POSITION = 0.8f;

class Ball
{
private:
	Vector2 velocity;
	Vector2 position;
	float radius;
	Texture2D* texture;
	Color color;
	bool active;
public:
	Ball();
	void update();
	void draw(int ballNumber);
	Vector2 getVelocity();
	void setVelocity(Vector2 vel);
	Vector2 getPosition();
	void setPosition(Vector2 pos);
	float getRadius();
	void setRadius(float rad);
	Texture2D* getTexture();
	void setTexture(Texture2D* tex);
	Color getColor();
	void setColor(Color col);
	bool getActive();
	void setActive(bool act);
	void hitBall(Vector2 mousePosition);
};

void SetBallsStartingConditions(Ball* balls[16], const int BALLS_AMMOUNT);

#endif
