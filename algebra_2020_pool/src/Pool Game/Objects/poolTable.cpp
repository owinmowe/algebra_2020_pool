#include "poolTable.h"

PoolTable::PoolTable()
{
	height = 0;
	width = 0;
	x = 0;
	y = 0;
	for (short i = 0; i < POOL_TABLE_HOLES; i++)
	{
		holes[i] = NULL;
		borders[i] = NULL;
	}
	backboard = {0,0,0,0};

	init();
}

PoolTable::~PoolTable()
{
	for (short i = 0; i < POOL_TABLE_HOLES; i++)
	{
		if (holes[i]!=NULL)
		{
			delete holes[i];
			holes[i] = NULL;
		}

		if (borders[i] != NULL)
		{
			delete borders[i];
			borders[i] = NULL;
		}		
	}
}

int PoolTable::getHeight()
{
	return height;
}

void PoolTable::setHeight(int h)
{
	height = h;
}

int PoolTable::getWidth()
{
	return width;
}

void PoolTable::setWidth(int w)
{
	width = w;
}

int PoolTable::getX()
{
	return x;
}

void PoolTable::setX(int _x)
{
	x = _x;
}

int PoolTable::getY()
{
	return y;
}

void PoolTable::setY(int _y)
{
	y = _y;
}

Rectangle PoolTable::getBackboard()
{
	return backboard;
}

void PoolTable::setBackboard(Rectangle b)
{
	backboard = b;
}

void PoolTable::initHoles()
{
	for (short i = 0; i < POOL_TABLE_HOLES; i++)
	{
		holes[i]->setRadius(screenWidth / 40);
		holes[i]->setColor(POOL_TABLE_HOLES_COLOR);
	}	
	
	holes[0]->setPosition({ POOL_TABLE.x + holes[0]->getRadius(),POOL_TABLE.y + holes[0]->getRadius() });
	
	holes[1]->setPosition({ POOL_TABLE.x + POOL_TABLE.width / 2,POOL_TABLE.y + holes[1]->getRadius() });

	holes[2]->setPosition({ POOL_TABLE.x + POOL_TABLE.width - holes[2]->getRadius() ,POOL_TABLE.y + holes[2]->getRadius() });

	holes[3]->setPosition({ POOL_TABLE.x + holes[3]->getRadius(),POOL_TABLE.y + POOL_TABLE.height - holes[3]->getRadius() });

	holes[4]->setPosition({ POOL_TABLE.x + POOL_TABLE.width / 2,POOL_TABLE.y + POOL_TABLE.height - holes[4]->getRadius() });

	holes[5]->setPosition({ POOL_TABLE.x + POOL_TABLE.width - holes[5]->getRadius() ,POOL_TABLE.y + POOL_TABLE.height - holes[5]->getRadius() });

}

Hole* PoolTable::getHoles(int i)
{
	return holes[i];
}

void PoolTable::initBorders()
{
	for (short i = 0; i < POOL_TABLE_BORDERS; i++)
	{
		borders[i]->setColor(POOL_TABLE_BORDERS_COLOR);
	}

	borders[0]->setCollider({ POOL_TABLE.x + holes[0]->getRadius() * 2, POOL_TABLE.y, holes[1]->getPosition().x - holes[0]->getPosition().x - holes[0]->getRadius() * 2, holes[0]->getRadius() * 1.2f });

	borders[1]->setCollider({ POOL_TABLE.x + POOL_TABLE.width / 2 + holes[0]->getRadius(), POOL_TABLE.y, holes[2]->getPosition().x - holes[1]->getPosition().x - holes[0]->getRadius() * 2, holes[0]->getRadius() * 1.2f });

	borders[2]->setCollider({ POOL_TABLE.x, POOL_TABLE.y + holes[0]->getRadius() * 2, holes[0]->getRadius() * 1.2f, holes[3]->getPosition().y - holes[0]->getPosition().y - holes[0]->getRadius() * 2 });

	borders[3]->setCollider({ POOL_TABLE.x + holes[0]->getRadius() * 2, POOL_TABLE.y + POOL_TABLE.height - holes[0]->getRadius() * 1.2f, holes[1]->getPosition().x - holes[0]->getPosition().x - holes[0]->getRadius() * 2, holes[0]->getRadius() * 1.2f });

	borders[4]->setCollider({ POOL_TABLE.x + POOL_TABLE.width / 2 + holes[0]->getRadius(), POOL_TABLE.y + POOL_TABLE.height - holes[0]->getRadius() * 1.2f, holes[2]->getPosition().x - holes[1]->getPosition().x - holes[0]->getRadius() * 2, holes[0]->getRadius() * 1.2f });

	borders[5]->setCollider({ POOL_TABLE.x + POOL_TABLE.width - holes[0]->getRadius() * 1.2f, POOL_TABLE.y + holes[0]->getRadius() * 2, holes[0]->getRadius() * 1.2f, holes[3]->getPosition().y - holes[0]->getPosition().y - holes[0]->getRadius() * 2 });
}

Border* PoolTable::getBorders(int i)
{
	return borders[i];
}

void PoolTable::init()
{
	for (short i = 0; i < POOL_TABLE_HOLES; i++)
	{
		holes[i] = new Hole();
		borders[i] = new Border();
	}
	initHoles();
	initBorders();
	setBackboard(POOL_TABLE);
}

void PoolTable::draw()
{
	DrawRectangleRec(getBackboard(), POOL_TABLE_BACKBOARD_COLOR);

	for (short i = 0; i < POOL_TABLE_HOLES; i++)
	{
		if (holes[i]!=NULL)
		{
			DrawCircleV(holes[i]->getPosition(), holes[i]->getRadius(), holes[i]->getColor());
		}

		if (borders[i]!=NULL)
		{			
			DrawRectangleRec(borders[i]->getCollider(), borders[i]->getColor());
			DrawText(TextFormat("%i", i + 1), static_cast<int>(borders[i]->getCollider().x + borders[i]->getCollider().width / 2),
				static_cast<int>(borders[i]->getCollider().y + borders[i]->getCollider().height / 2), 20, BLACK);
		}
	}
}

