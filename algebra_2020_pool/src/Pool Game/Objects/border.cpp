#include "border.h"

Border::Border()
{
	collider = {0,0,0,0};
	color = WHITE;
}

Border::~Border()
{

}

Rectangle Border::getCollider()
{
	return collider;
}

void Border::setCollider(Rectangle coll)
{
	collider = coll;
}

Color Border::getColor()
{
	return color;
}

void Border::setColor(Color col)
{
	color = col;
}