#include "poolGame.h"
#include "raylib.h"
#include "Collision/collision.h"
#include "General/screenConfig.h"
#include "Objects/poolTable.h"
#include "Objects/ball.h"

PoolGame::PoolGame()
{
    InitWindow(screenWidth, screenHeight, "Pool");
    SetTargetFPS(60);
    poolTable = new PoolTable();
    resetGame();
}
PoolGame::~PoolGame()
{
    for (int i = 0; i < BALLS_AMMOUNT; i++)
    {
        delete balls[i];
    }
    delete poolTable;
    CloseWindow();
}
void PoolGame::play()
{
    while (!WindowShouldClose()&& !gameOver)
    {
        input();
        update();
        draw();  
    }
}
void PoolGame::input()
{
    Vector2 mousePoint = GetMousePosition();
    if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
    {
        if(!win){ balls[0]->hitBall(mousePoint); }
    }
    else if (win && IsKeyPressed(KEY_R))
    {
        resetGame();
    }
#if DEBUG 
    if(IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)) //Collision tester
    {
        for (int i = 0; i < BALLS_AMMOUNT; i++)
        {
            balls[i]->hitBall(mousePoint);
        }
    }
    if(IsMouseButtonPressed(MOUSE_MIDDLE_BUTTON))
    {
        for (int i = 0; i < BALLS_AMMOUNT; i++)
        {
            balls[i] = new Ball;
        }
        SetBallsStartingConditions(balls, BALLS_AMMOUNT);
    }
#endif // DEBUG
}
void PoolGame::resetGame()
{
    for (int i = 0; i < BALLS_AMMOUNT; i++)
    {
        balls[i] = new Ball;
    }
    SetBallsStartingConditions(balls, BALLS_AMMOUNT);
    gameOver = false;
    win = false;
}
void PoolGame::update()
{
    
    Hole* auxHoles[POOL_TABLE_HOLES];  
    Border* auxBorders[POOL_TABLE_BORDERS];    

    for (short i = 0; i < 6; i++)
    {
        auxHoles[i] = new Hole();
        auxHoles[i] = poolTable->getHoles(i);
        auxBorders[i] = new Border();
        auxBorders[i] = poolTable->getBorders(i);
    }   
    
    for (int i = 0; i < BALLS_AMMOUNT; i++)
    {
        balls[i]->update();
        PoolTableCollision(auxBorders,balls[i]);
        PoolTableHolesCollision(auxHoles,balls[i]);
        BallsCollision(balls, BALLS_AMMOUNT, i);
    }

    if (!balls[0]->getActive())
    {
        Vector2 aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * WHITE_BALL_STARTING_POSITION, screenHeight / 2 };
        balls[0]->setPosition(aux);
        balls[0]->setActive(true);
    }

    win = gameOverCheck();
}
void PoolGame::draw()
{
    BeginDrawing();
    ClearBackground(RAYWHITE); 
    poolTable->draw();

    Vector2 mousePoint = GetMousePosition();
    if (IsMouseButtonDown(MOUSE_LEFT_BUTTON) && !win)
    {
        DrawLine(static_cast<int>(mousePoint.x), static_cast<int>(mousePoint.y), static_cast<int>(balls[0]->getPosition().x), static_cast<int>(balls[0]->getPosition().y), BLACK);
    }

    for (int i = 0; i < BALLS_AMMOUNT; i++)
    {
        balls[i]->draw(i);
    }

    if (win)
    {
        DrawText("Winner", screenWidth / 2 - MeasureText("Winner", screenHeight / 40) / 2, screenHeight / 2, screenHeight / 40, ORANGE);
        DrawText("Press R key to reset the game", screenWidth / 2 - MeasureText("Press R key to reset the game", screenHeight / 40) / 2, screenHeight / 2 + 50, screenHeight / 40, ORANGE);
    }
   
   
    EndDrawing();
}
bool PoolGame::gameOverCheck()
{
    if (!balls[8]->getActive())
    {
        for (short i = 1; i < BALLS_AMMOUNT; i++)
        {
            if (balls[i]->getActive())
            {
                i = BALLS_AMMOUNT;
                return true;
            }                    
        }
        return false;
    } 
    else
    {
        return false;
    }
}